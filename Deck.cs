using System;
using System.Collections.Generic;
using System.Linq;

namespace SetGame {
	public class Deck {

		static readonly Random Random = new Random();

		public void Shuffle() {

			for (int i = 0; i < 1000; i++) {
				var first = Random.Next(Cards.Count);
				var second = Random.Next(Cards.Count);

				var temp = Cards[first];
				Cards[first] = Cards[second];
				Cards[second] = temp;
			}

		}

		public List<Card> Cards = new List<Card>();

		public Deck() {
			var allShapes = new[] { Shape.Diamond, Shape.Oval, Shape.Squiggly };
			var allColors = new[] { Color.Green, Color.Purple, Color.Red };
			var allshades = new[] { Shade.Striped, Shade.Hollow, Shade.Solid };
			foreach (var s in allShapes) {
				foreach (var c in allColors) {
					foreach (var sh in allshades) {
						for (int i = 1; i <= 3; i++) {
							Cards.Add(new Card(s, c, i, sh));
						}
					}
				}
			}
		}
	}
}

