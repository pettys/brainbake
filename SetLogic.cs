using System;
using System.Collections.Generic;
using System.Linq;

namespace SetGame {

	public class SetLogic {

		public static List<Card[]> FindAllSets(Card[] cards) {
			var matches = new List<Card[]>();
			for (var x = 0; x < cards.Length; x++) {
				for (var y = x + 1; y < cards.Length; y++) {
					for (var z = y + 1; z < cards.Length; z++) {
						if (IsMatch(cards[x], cards[y], cards[z])) {
							matches.Add(new[] { cards[x], cards[y], cards[z] });
						}
					}
				}
			}
			return matches;
		}

		private static bool IsMatch(Card x, Card y, Card z) {
			return IsAttributeMatch((int)x.Shape, (int)y.Shape, (int)z.Shape)
				&& IsAttributeMatch((int)x.Color, (int)y.Color, (int)z.Color)
				&& IsAttributeMatch((int)x.Shade, (int)y.Shade, (int)z.Shade)
				&& IsAttributeMatch(x.Count, y.Count, z.Count);
		}

		private static bool IsAttributeMatch(int x, int y, int z) {
			if (x == y && y == z) {
				return true;
			}
			if (x != y && x != z && y != z) {
				return true;
			}
			return false;
		}

	}
}

