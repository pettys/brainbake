﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SetGame {

	public class Program {

		static void Main(string[] args) {

			var deck = new Deck();
			deck.Shuffle();
			int cardsOnTable = 20;

			int i = 1;
			foreach (var card in deck.Cards.Take(cardsOnTable)) {
				Console.ForegroundColor = ConsoleColor.White;
				Console.Write(string.Format("{0,2} ", i++));
				card.Print();
				Console.WriteLine();
			}

			Console.ReadLine();

			Console.ForegroundColor = ConsoleColor.White;
			var matches = SetLogic.FindAllSets(deck.Cards.Take(cardsOnTable).ToArray());
			Console.WriteLine();
			Console.WriteLine("Matches: ");
			foreach (var match in matches) {
				foreach (var matchCard in match) {
					matchCard.Print();
					Console.Write(" ");
				}
				Console.WriteLine();
			}

			Console.ForegroundColor = ConsoleColor.Yellow;
			Console.WriteLine();
			Console.WriteLine();
			Console.Write("That's all!");
			Console.ReadLine();

		}

	}

}
