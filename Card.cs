﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace SetGame {
	public class Card {

		public Shape Shape { get; private set; }
		public Color Color { get; private set; }
		public int Count { get; private set; }
		public Shade Shade { get; private set; }

		public static int Whatever;

		public Card(Shape shape, Color color, int count, Shade shade) {
			Shape = shape;
			Color = color;
			Count = count;
			Shade = shade;
		}

		public void Print() {
			switch (Color) {
				case SetGame.Color.Green: Console.ForegroundColor = ConsoleColor.Green; break;
				case SetGame.Color.Purple: Console.ForegroundColor = ConsoleColor.Magenta; break;
				case SetGame.Color.Red: Console.ForegroundColor = ConsoleColor.Red; break;
			}
			Console.Write("[{0,-3} {1}]", ShapeAndCount, ShadeCharacter);
		}

		public char ShadeCharacter {
			get {
				switch (Shade) {
					case SetGame.Shade.Solid: return '█';
					case SetGame.Shade.Striped: return '░';
					default: return ' ';
				}
			}
		}

		public char ShapeCharacter {
			get {
				switch (Shape) {
					case SetGame.Shape.Oval: return 'O';
					case SetGame.Shape.Squiggly: return '~';
					case SetGame.Shape.Diamond: return (char)4;
					default: return '?';
				}
			}
		}

		public string ShapeAndCount {
			get {
				return new string(ShapeCharacter, Count);
			}
		}

	}
}

